FROM ubuntu:18.04

MAINTAINER fabio.sturm@gmail.com

WORKDIR /

RUN apt-get update && \
    apt-get install -y git-core curl unzip make lcov && \
    git clone --branch v1.10.5 --depth=1 https://github.com/flutter/flutter.git && \
    /flutter/bin/flutter doctor && \
    /flutter/bin/flutter config --enable-web && \
    apt-get remove -y curl unzip && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

ENV PATH $PATH:/flutter/bin/cache/dart-sdk/bin:/flutter/bin
