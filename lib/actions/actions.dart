import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:regatta_flutter/models/app_state.dart';
import 'package:regatta_flutter/models/event.dart';
import 'package:regatta_flutter/services/database_service.dart';
import 'package:regatta_flutter/services/service_locator.dart';

ThunkAction<AppState> requestFilteredEventsAction() {
  return (Store<AppState> store) async {
    List<Event> events = sl<DatabaseService>().get_events();
    for (var event in events) {
      store.dispatch(AddEventAction(event));
    }
    store.dispatch(
        FilteredEventsAction(events.map((event) => event.key).toList()));
  };
}

class FilteredEventsAction {
  final List<String> event_keys;
  FilteredEventsAction(this.event_keys);
}

class EventsLoadedAction {
  final List<Event> events;
  EventsLoadedAction(this.events);
}

class AddEventAction {
  final Event event;
  AddEventAction(this.event);
}

ThunkAction<AppState> requestUpdateEventAction(Event event) {
  return (Store<AppState> store) async {
    // FIXME: add async call Event updatedEvent = await updateEvent(event)
    store.dispatch(UpdateEventAction(event));
  };
}

class UpdateEventAction {
  final Event event;
  UpdateEventAction(this.event);
}

class RemoveEventAction {
  final String key;
  RemoveEventAction(this.key);
}
