import 'package:flutter/material.dart';
import 'package:regatta_flutter/containers/event_container.dart';
import 'package:regatta_flutter/containers/filtered_boats.dart';
import 'package:regatta_flutter/containers/filtered_events.dart';

class DrawerItem {
  final String title;
  final String route;
  final IconData icon;
  const DrawerItem(this.title, this.route, this.icon);
}

class HomePage extends StatefulWidget {
  HomePage({super.key, required this.title});

  final String title;

  final drawerItems = [
    DrawerItem("Events", "events", Icons.event),
    DrawerItem("Boats", "boats", Icons.send),
    DrawerItem("SailingClubs", "sailingclubs", Icons.supervisor_account),
    DrawerItem("Settings", "settings", Icons.settings),
  ];

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  final navigatorKey = GlobalKey<NavigatorState>();
  int _selectedDrawerIndex = 0;

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
    navigatorKey.currentState
        ?.pushNamed(widget.drawerItems[_selectedDrawerIndex].route);
  }

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(ListTile(
        leading: Icon(d.icon),
        title: Text(d.title),
        selected: i == _selectedDrawerIndex,
        onTap: () => _onSelectItem(i),
      ));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            Column(children: drawerOptions)
          ],
        ),
      ),
      body: Navigator(
          key: navigatorKey,
          initialRoute: 'events',
          onGenerateRoute: (RouteSettings settings) {
            WidgetBuilder builder;
            switch (settings.name) {
              case 'events':
                builder = (BuildContext _) => FilteredEvents();
                break;
              case 'event':
                builder =
                    (BuildContext _) => EventContainer(settings.arguments as String);
                break;
              case 'boats':
                builder = (BuildContext _) => FilteredBoats();
                break;
              default:
                throw Exception('Invalid route: ${settings.name}');
            }
            return MaterialPageRoute(builder: builder, settings: settings);
          }),
    );
  }
}
