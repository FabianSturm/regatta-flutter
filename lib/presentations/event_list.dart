import 'package:flutter/material.dart';
import 'package:regatta_flutter/models/models.dart';

class EventList extends StatelessWidget {
  final List<Event> events;
  final Function(Event) onAddEvent;

  const EventList({
    final Key? key,
    required this.events,
    required this.onAddEvent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(30.0),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(children: <Widget>[
          Row(children: <Widget>[
            Expanded(
              child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search), hintText: 'Search...'),
              ),
            ),
            FloatingActionButton(
              onPressed: () {
                onAddEvent(Event('test'));
              },
              child: Icon(Icons.add),
              backgroundColor: Colors.deepOrange,
              mini: true,
            ),
          ]),
          Expanded(
            child: ListView.builder(
              // FIXME: useListView.separated for divider
              itemCount: events.length,
              itemBuilder: (BuildContext context, int index) {
                final Event event = events[index];
                return ListTile(
                    leading: Icon(Icons.web),
                    title: Text(event!.name ?? ""),
                    subtitle: Text(
                        'SCW, ${event.startDateStr} until ${event.endDateStr}, 12 participants'),
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed('event', arguments: event.key);
                    });
              },
            ),
          ),
        ]),
      ),
    );
  }
}
