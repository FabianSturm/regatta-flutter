import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:regatta_flutter/models/event.dart';

class EventDetails extends StatelessWidget {
  final Event event;
  final Function(Event) onUpdateEvent;

  // FIXME: remove after https://github.com/flutter/flutter/issues/35435 is fixed
  final TextEditingController nameController;

  /*const*/ EventDetails({
    // FIXME: reactivate after bugfix
    required this.event,
    required this.onUpdateEvent,
  }) : nameController = TextEditingController(text: event.name);

  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(30.0),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 30.0),
              child: Column(
                children: <Widget>[
                  // FIXME: remove after bugfix
                  RawKeyboardListener(
                    focusNode: FocusNode(),
                    onKey: (keyevent) {
                      if (keyevent is RawKeyUpEvent &&
                          keyevent.data is RawKeyEventDataWeb) {
                        var data = keyevent.data as RawKeyEventDataWeb;
                        if (data.code == 'Enter') {
                          onUpdateEvent(
                              event.copyWith(name: nameController.text));
                        }
                      }
                    },
                    child: TextField(
                      decoration: InputDecoration(
                        labelText: 'Name',
                      ),
                      controller: nameController,
                      /* TextEditingController(text: event.name), */
                      onSubmitted: (String value) {
                        print(
                            "FIXME: submitted now works, remove RawKeyboardListener");
                      },
                      maxLines: 1,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          decoration: InputDecoration(
                            labelText: 'Start date',
                          ),
                          controller:
                              TextEditingController(text: event.startDateStr),
                          onTap: () => _selectStartDate(context),
                        ),
                      ),
                      Expanded(
                        child: TextField(
                            decoration: InputDecoration(
                          labelText: 'End date',
                        ),
                          controller: TextEditingController(text: event.endDateStr),
                          onTap: () => _selectEndDate(context),
                        ),
                      )
                    ],
                  ),
                  TextField(
                    decoration: InputDecoration(
                      labelText: 'Race count',
                    ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                      labelText: 'Race unrated on',
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 8.0, 30.0, 0.0),
            child: Text(
              'Entities',
              style: TextStyle(fontSize: 17.0),
            ),
          ),
          Card(
            margin: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 30.0),
            child: Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 30.0),
                child: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                        labelText: 'Organizer',
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        labelText: 'Race committee',
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        labelText: 'Umpire',
                      ),
                    ),
                  ],
                )),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 8.0, 30.0, 0.0),
            child: Text(
              'Fees',
              style: TextStyle(fontSize: 17.0),
            ),
          ),
          Card(
            margin: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 30.0),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 30.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(
                      labelText: 'Amount',
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<Null> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: event.startDate,
        firstDate: DateTime(2000, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != event.startDate) {
      onUpdateEvent(event.copyWith(startDate: picked));
    }
  }

  Future<Null> _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: event.endDate,
        firstDate: DateTime(2000, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != event.endDate) {
      onUpdateEvent(event.copyWith(endDate: picked));
    }
  }

}
