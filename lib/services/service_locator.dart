import 'package:get_it/get_it.dart';
import 'package:regatta_flutter/services/database_service.dart';

GetIt sl = GetIt.instance;
void setup_services() {
  sl.registerSingleton<DatabaseService>(DatabaseService());
}
