import 'package:regatta_flutter/models/event.dart';

class DatabaseService {
  List<Event> get_events() {
    return [
      Event('1', name: 'Regatta A'),
      Event('2', name: 'Regatta B'),
    ];
  }
}
