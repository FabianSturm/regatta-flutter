import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:regatta_flutter/actions/actions.dart';
import 'package:regatta_flutter/models/app_state.dart';
import 'package:regatta_flutter/models/models.dart';
import 'package:regatta_flutter/presentations/event_details.dart';

class EventContainer extends StatelessWidget {
  final String event_key;

  EventContainer(this.event_key);

  Widget build(BuildContext context) {
    return StoreConnector(
        converter: _ViewModel.fromStore(event_key),
        builder: (context, vm) {
          return DefaultTabController(
              length: 6,
              child: Column(children: <Widget>[
                TabBar(
                    unselectedLabelColor: Colors.grey.shade700,
                    labelColor: Colors.blue,
                    tabs: [
                      Tab(text: 'DETAILS'),
                      Tab(text: 'PARTICIPANTS'),
                      Tab(text: 'GROUPS'),
                      Tab(text: 'RACES'),
                      Tab(text: 'RESULTS'),
                      Tab(text: 'LISTS'),
                    ]),
                Expanded(
                  child: TabBarView(
                    children: [
                      EventDetails(
                        event: vm.event,
                        onUpdateEvent: vm.onUpdateEvent,
                      ),
                      Icon(Icons.directions_transit),
                      Icon(Icons.directions_bike),
                      Icon(Icons.directions_bike),
                      Icon(Icons.directions_bike),
                      Icon(Icons.directions_bike),
                    ],
                  ),
                ),
              ]));
        });
  }
}

class _ViewModel {
  final Event event;
  final Function(Event) onUpdateEvent;

  _ViewModel({
    required this.event,
    required this.onUpdateEvent,
  });

  static _ViewModel Function(Store<AppState> store) fromStore(
      String event_key) {
    return (Store<AppState> store) {
      return _ViewModel(
        event: store.state.events[event_key]!,
        onUpdateEvent: (event) {
          store.dispatch(requestUpdateEventAction(event));
        },
      );
    };
  }
}
