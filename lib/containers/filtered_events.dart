import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:regatta_flutter/actions/actions.dart';
import 'package:regatta_flutter/models/app_state.dart';
import 'package:regatta_flutter/models/models.dart';
import 'package:regatta_flutter/presentations/event_list.dart';

class FilteredEvents extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector(
      converter: _ViewModel.fromStore,
      builder: (context, vm) {
        return EventList(
          events: vm.events,
          onAddEvent: vm.onAddEvent,
        );
      },
    );
  }
}

class _ViewModel {
  final List<Event> events;
  final Function(Event) onAddEvent;

  _ViewModel({
    required this.events,
    required this.onAddEvent,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      events: store.state.filteredEvents
          .where((key) => store.state.events.containsKey(key))
          .map((key) => store.state.events[key]!)
          .toList(),
      onAddEvent: (event) {
        store.dispatch(AddEventAction(event));
      },
    );
  }
}
