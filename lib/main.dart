import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:regatta_flutter/actions/actions.dart';
import 'package:regatta_flutter/models/models.dart';
import 'package:regatta_flutter/presentations/home_page.dart';
import 'package:regatta_flutter/reducers/app_reducer.dart';
import 'package:regatta_flutter/services/service_locator.dart';

void main() {
  setup_services();
  runApp(ReduxApp());
}

class ReduxApp extends StatelessWidget {
  final Store<AppState> store;

  ReduxApp()
      : store = Store<AppState>(
          appReducer,
          initialState: AppState(),
          middleware: [
            thunkMiddleware,
            LoggingMiddleware.printer(),
          ],
        ) {
    print('Initial state: ${store.state}');
    store.dispatch(requestFilteredEventsAction());
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        title: 'Regatta: The sailing race scoring program',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(title: 'Regatta'),
      ),
    );
  }
}
