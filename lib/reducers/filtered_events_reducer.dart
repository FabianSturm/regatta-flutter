import 'package:redux/redux.dart';
import 'package:regatta_flutter/actions/actions.dart';

final filteredEventsReducer = combineReducers<List<String>>([
  TypedReducer<List<String>, RemoveEventAction>(_removeEvent),
  TypedReducer<List<String>, FilteredEventsAction>(_filteredEvents),
]);

List<String> _removeEvent(
    List<String> filteredEvents, RemoveEventAction action) {
  return List.from(filteredEvents)..remove(action.key);
}

List<String> _filteredEvents(
    List<String> filteredEvents, FilteredEventsAction action) {
  return List.from(action.event_keys);
}
