import 'package:redux/redux.dart';
import 'package:regatta_flutter/actions/actions.dart';
import 'package:regatta_flutter/models/event.dart';

final eventsReducer = combineReducers<Map<String, Event>>([
  TypedReducer<Map<String, Event>, AddEventAction>(_addEvent),
  TypedReducer<Map<String, Event>, UpdateEventAction>(_updateEvent),
  TypedReducer<Map<String, Event>, RemoveEventAction>(_removeEvent),
]);

Map<String, Event> _addEvent(Map<String, Event> events, AddEventAction action) {
  return Map.from(events)..addAll({action.event.key: action.event});
}

Map<String, Event> _updateEvent(
    Map<String, Event> events, UpdateEventAction action) {
  return Map.from(events)..addAll({action.event.key: action.event});
}

Map<String, Event> _removeEvent(
    Map<String, Event> events, RemoveEventAction action) {
  return Map.from(events)..remove(action.key);
}
