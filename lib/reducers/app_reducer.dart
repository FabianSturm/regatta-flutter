import 'package:regatta_flutter/models/models.dart';
import 'package:regatta_flutter/reducers/events_reducer.dart';
import 'package:regatta_flutter/reducers/filtered_events_reducer.dart';

AppState appReducer(AppState state, action) {
  return AppState(
    events: eventsReducer(state.events, action),
    filteredEvents: filteredEventsReducer(state.filteredEvents, action),
  );
}
