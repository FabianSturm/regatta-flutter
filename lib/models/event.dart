import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

@immutable
class Event {
  final String key;
  final String? name;
  final DateTime startDate;
  final DateTime endDate;
  final int? raceCount;
  //final String? raceUnratedOn;
  //final String? organizer;
  //final String? raceCommittee;
  //final String? umpire;
  //final List<String>? assistants;
  // entries
  // races

  Event(
    this.key, {
    this.name,
    this.raceCount,
    startDate,
    endDate,
  })  : this.startDate = startDate ?? DateTime.now(),
        this.endDate = endDate ?? DateTime.now();

  @override
  String toString() {
    return 'Event{name: $name}';
  }

  String get startDateStr {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    return formatter.format(startDate);
  }

  String get endDateStr {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    return formatter.format(endDate);
  }

  /// Clones this instance
  Event copyWith({
    String? key,
    String? name,
    DateTime? startDate,
    DateTime? endDate,
    int? raceCount,
  }) {
    return Event(
      key ?? this.key,
      name: name ?? this.name,
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
      raceCount: raceCount ?? this.raceCount,
    );
  }
}
