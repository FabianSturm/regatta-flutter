import 'package:meta/meta.dart';
import 'package:regatta_flutter/models/models.dart';

@immutable
class AppState {
  final Map<String, Event> events;
  final List<String> filteredEvents;

  const AppState({
    this.events = const {},
    this.filteredEvents = const [],
  });

  @override
  String toString() {
    return 'AppState: {events: $events, filteredEvents: $filteredEvents}';
  }
}
