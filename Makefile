# Make sure that flutter is in the path
# export PATH=$PATH:~/flutter/bin

SHELL = /bin/bash

BRANCH := $(shell git for-each-ref --format='%(objectname) %(refname:short)' refs/heads | awk "/^$$(git rev-parse HEAD)/ {print \$$2}")
HASH := $(shell git rev-parse HEAD)

all: get run

install:
	flutter channel master
	flutter upgrade
	flutter config --enable-web

get:
	flutter pub get

run:
	flutter run -d chrome
	# flutter pub global run webdev serve --auto restart

test:
	flutter test --coverage
	genhtml coverage/lcov.info --output=coverage

build:
	flutter build web

docker:
	docker build . -t flutter_dev:0.1

.PHONY: install get run test build docker
